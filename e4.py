# Ejercicio 4 tarea
#__author__ = "Juan     Alvarez"
#__email_ = "juan.v.alvarez@unl.edu.ec"
#Ejercicio 4: Asume que ejecutamos las siguientes sentencias de asignación

ancho = 17
alto = 12.0
#Para cada una de las expresiones siguientes, escribe el valor de la expresión y el tipo (del valor de la expresión).
v1 = ancho/2
v2 = ancho/2.0
v3 = alto/3
v4 = 1 + 2 * 5
print (v1)
print (type(v1))
print (v2)
print (type(v2))
print (v3)
print (type(v3))
print (v4)
print (type(v4))